package com.silverline.Gateway.BaseTests;

import com.silverline.Gateway.Helpers.BrowsersHelper;
import com.silverline.Gateway.Methods.MainMethods;
import com.silverline.Gateway.PO.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class BaseTests {

    public static WebDriver driver;
    public static LoginPage lp;
    public static MainMethods mm;
    public static HomePage hp;
    public static ProjectsTab pt;
    public static DashboardTab dt;
    public static ProjectPage pp;
    public static ForgotPasswordPage fpp;

    @BeforeClass
    public static void setup() {
        //System.setProperty("webdriver.chrome.driver", "D:\\2018_!!!\\comsilverlineCafs\\chromedriver.exe");
        //driver = new ChromeDriver();
        //driver = BrowsersHelper.getChromeLocalWebDriver();
        driver = BrowsersHelper.getChromeRemoteWebDriver();
        lp = new LoginPage(driver);
        mm = new MainMethods(driver);
        hp = new HomePage(driver);
        pt = new ProjectsTab(driver);
        dt = new DashboardTab(driver);
        pp = new ProjectPage(driver);
        fpp = new ForgotPasswordPage(driver);
        driver.manage().window().maximize();
        driver.get("https://gateway.silverlinecrm.com");
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        lp.LoginToApplication();
    }

    @AfterClass
    public static void end() {
        driver.quit();
    }
}
