package com.silverline.Gateway.PO;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class ProjectPage extends BaseTests{

    public ProjectPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    String errorText = "Error: You must enter a value";

    //---Locators

    @FindBy(css=".slds-size_7-of-12.slds-p-top_large.slds-p-horizontal_medium")
    public WebElement ProjectPage;

    @FindBy(xpath="//li[@class='slds-nav-vertical__item InactivefontInfo'][1]/a[@class='slds-nav-vertical__action anchorTag']")
    public WebElement BacklogTab;

    @FindBy(xpath="//span[@class='AddNewText']")
    public WebElement AddNewUserStoryButton;

    @FindBy(xpath="//span[@class='AddNewText']")
    public WebElement AddNewButton;

    @FindBy(css=".slds-dropdown.slds-dropdown_left.slds-dropdown_actions.AddNewMenuSectionDIV.cSL_CP_PageTitle")
    public WebElement UserStoryButton;

    //-----New User Story window
    @FindBy(xpath="//div[@class='borderStand']/input[@type='text']")
    public WebElement StorySubjectField;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div[1]/div/div[@class='MandatoryText']")
    public WebElement ErrorMessageStorySubjectField;

    @FindBy(xpath="//div[@class='borderStand']/div//input[@type='text']")
    public WebElement EpicField;

    @FindBy(css=".slds-listbox.slds-listbox_vertical.slds-dropdown.slds-dropdown_fluid.slds-lookup__menu.slds")
    public WebElement EpicFirstValue;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div/div[1]/div[1]/div[1]/div[@class='MandatoryText']")
    public WebElement ErrorMessageEpicField;

    @FindBy(css=".inputCustomLookup.input")
    public WebElement ActorField;

    @FindBy(css=".uiOutputText")
    public WebElement ActorFirstValue;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div/div[1]/div[1]/div[2]/div[@class='MandatoryText']")
    public WebElement ErrorMessageActorField;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div[2]/div[1]/div[2]/div/div[@class='borderStand']/textarea[@role='textbox']")
    public WebElement TaskField;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div/div[1]/div[2]/div/div[@class='MandatoryText']")
    public WebElement ErrorMessageTaskField;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div[2]/div[1]/div[3]/div/div[@class='borderStand']/textarea[@role='textbox']")
    public WebElement BusinessBenefitField;

    @FindBy(xpath="/html//div[@id='modal-content-id-1']/div/div[1]/div[3]/div/div[@class='MandatoryText']")
    public WebElement ErrorMessageBusinessBenefitField;

    //-----Buttons
    @FindBy(css=".slds-modal__footer .CancelButtonCSS:nth-of-type(1) .CancelTextCSS")
    public WebElement CancelButton;

    @FindBy(xpath="//section[@role='dialog']//span[@class='SaveTextCSS']")
    public WebElement SaveButton;

    @FindBy(xpath="//section[@role='dialog']//span[@class='SaveNewTextCSS']")
    public WebElement SaveAndNewButton;


    //-----METHODS

    public void OpenProjectBacklog (){
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
        pp.BacklogTab.sendKeys(Keys.ENTER);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AddNewButton.isDisplayed();
    }

    public void OpenNewUserStory (){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        AddNewButton.click();
        UserStoryButton.isDisplayed();
        UserStoryButton.click();
        StorySubjectField.isDisplayed();
    }

    public void ValidateErrorMessage (WebElement error){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        error.isDisplayed();
        Assert.assertTrue("Incorrect Error message. Expected:"+errorText+", Actual:"+error.getText(), error.getText().equals(errorText));
    }

    public void FillEpicField (String value){
        mm.FillField(pp.EpicField, value);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        pp.EpicFirstValue.click();
    }

    public void FillSearchField (String value) throws InterruptedException {
        mm.FillField(pp.ActorField, value);
        Thread.sleep(5000);
        pp.ActorFirstValue.click();
    }

    public void OpenNewUserStoryWindow(){
        driver.get("https://gateway.silverlinecrm.com/s/");
        dt.OpenFirstProject();
        pp.OpenProjectBacklog();
        pp.OpenNewUserStory();
    }
}
