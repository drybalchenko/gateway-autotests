package com.silverline.Gateway.PO;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ProjectsTab extends BaseTests {

    public ProjectsTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }
}
