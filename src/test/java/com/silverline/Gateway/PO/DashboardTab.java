package com.silverline.Gateway.PO;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.silverline.Gateway.BaseTests.BaseTests.hp;
import static com.silverline.Gateway.BaseTests.BaseTests.mm;

public class DashboardTab extends BaseTests{

    public DashboardTab(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    @FindBy(xpath="//a[@title='Adviser Investments - Navigator 4/2018 - 3/2019']")
    public WebElement FirstProjectName;

    //-----Methods
    public void OpenFirstProject (){
        mm.ClickOn(FirstProjectName);
        pp.ProjectPage.isDisplayed();
    }
}
