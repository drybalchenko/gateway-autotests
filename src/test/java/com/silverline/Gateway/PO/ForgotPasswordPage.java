package com.silverline.Gateway.PO;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.silverline.Gateway.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.silverline.Gateway.BaseTests.BaseTests.hp;
import static com.silverline.Gateway.BaseTests.BaseTests.mm;

public class ForgotPasswordPage extends BaseTests{

    public ForgotPasswordPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    @FindBy(css = ".uiInput--input[placeholder='Email']")
    public WebElement EmailField;

    @FindBy(css = ".uiButton.submitButtonCSS")
    public WebElement SendResetInstructionsButton;

    @FindBy(xpath = "//span[@class='ReturnTextCSS']")
    public WebElement ReturnToLogInPageLink;

    public void NavigateToLogInPage(){
        mm.ClickOn(ReturnToLogInPageLink);
        lp.LoginButton.isDisplayed();
    }

}
