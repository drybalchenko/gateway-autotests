package com.silverline.Gateway.PO;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BaseTests{

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    @FindBy(xpath="//li[@id='Dashboard']")
    public WebElement DashboardTab;

    @FindBy(xpath="//li[@id='Projects']")
    public WebElement ProjectsTab;

    @FindBy(xpath="//a[@title='Active Projects']")
    public WebElement ActiveProjectsTab;

    @FindBy(xpath="//a[@title='Completed Projects']")
    public WebElement CompletedProjectsTab;

    //---Methods

    public void NavigateToDeshboardTab (){
        mm.ClickOn(hp.DashboardTab);
        dt.FirstProjectName.isDisplayed();
    }
}
