package com.silverline.Gateway.PO;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.silverline.Gateway.BaseTests.BaseTests.hp;
import static com.silverline.Gateway.BaseTests.BaseTests.mm;

public class LoginPage extends BaseTests{

    //public WebDriver driver;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    public String UserLogin = "dmitry.rybalchenko@silverlinecrm.com";
    public String UserPassword = "Fish@25051990";


    @FindBy(css = ".uiInput--input[placeholder='Email']")
    public WebElement LoginField;

    @FindBy(css = ".uiInput--input[placeholder='Password']")
    public WebElement PasswordField;

    @FindBy(css = ".uiButton.loginButton")
    public WebElement LoginButton;

    @FindBy(xpath = "//span[@class='forgotPasswordCSS']")
    public WebElement ForgotPasswordLink;


    public void LoginToApplication (){
        mm.FillField(LoginField, UserLogin);
        mm.FillField(PasswordField, UserPassword);
        mm.ClickOn(LoginButton);
        hp.DashboardTab.isDisplayed();
    }

    public void NavigateToForgotPassword(){
        mm.ClickOn(ForgotPasswordLink);
        fpp.SendResetInstructionsButton.isDisplayed();
    }
}
