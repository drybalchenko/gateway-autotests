package com.silverline.Gateway.Methods;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class MainMethods extends BaseTests{

    public MainMethods(WebDriver driver) {
        PageFactory.initElements(driver, this);
        BaseTests.driver = driver;
    }

    public void FillField(WebElement field, String value){
        field.isDisplayed();
        field.click();
        field.clear();
        field.sendKeys(value);
    }

    public void ClickOn (WebElement element){
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.MINUTES);
        element.isDisplayed();
        element.sendKeys(Keys.ENTER);
    }
}
