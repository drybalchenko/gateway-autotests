package com.silverline.Gateway.TestSuites;

import com.silverline.Gateway.Tests.CheckRequiredFields;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    CheckRequiredFields.class
})

public class RegressionTestSuite {
}
