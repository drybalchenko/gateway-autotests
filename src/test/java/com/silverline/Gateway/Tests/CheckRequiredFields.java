package com.silverline.Gateway.Tests;

import com.silverline.Gateway.BaseTests.BaseTests;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class CheckRequiredFields extends BaseTests{

    @Test
    public void GAT01_EmptyUserStorySaveButton(){
        System.out.println("[INFO]: Test 'GAT01 Save Empty User Story For 'Save' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        pp.SaveButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageActorField);
        pp.ValidateErrorMessage(pp.ErrorMessageBusinessBenefitField);
        pp.ValidateErrorMessage(pp.ErrorMessageEpicField);
        pp.ValidateErrorMessage(pp.ErrorMessageStorySubjectField);
        pp.ValidateErrorMessage(pp.ErrorMessageTaskField);
        System.out.println("[INFO]: Test 'GAT01 Save Empty User Story For 'Save' button' - PASSED <<<");
    }

    @Test
    public void GAT02_UserStoryWithoutSubjectSaveButton() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT02 Save User Story Without Subject For 'Save' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        pp.FillEpicField("Case Management");
        pp.FillSearchField("System");
        mm.FillField(pp.TaskField, "Test Text");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageStorySubjectField);
        System.out.println("[INFO]: Test 'GAT02 Save User Story Without Subject For 'Save' button' - PASSED <<<");
    }

    @Test
    public void GAT03_UserStoryWithoutEpicSaveButton() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT03 Save User Story Without Epic For 'Save' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillSearchField("System");
        mm.FillField(pp.TaskField, "Test Text");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageEpicField);
        System.out.println("[INFO]: Test 'GAT03 Save User Story Without Epic For 'Save' button' - PASSED <<<");
    }

    @Test
    public void GAT04_UserStoryWithoutActorSaveButton(){
        System.out.println("[INFO]: Test 'GAT04 Save User Story Without Actor For 'Save' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillEpicField("Case Management");
        mm.FillField(pp.TaskField, "Test Text");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageActorField);
        System.out.println("[INFO]: Test 'GAT04 Save User Story Without Actor For 'Save' button' - PASSED <<<");
    }

    @Test
    public void GAT05_UserStoryWithoutTaskSaveButton() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT05 Save User Story Without Task For 'Save' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillEpicField("Case Management");
        pp.FillSearchField("System");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageTaskField);
        System.out.println("[INFO]: Test 'GAT05 Save User Story Without Task For 'Save' button' - PASSED <<<");
    }

    @Test
    public void GAT06_UserStoryWithoutBusinessBenefitSaveButton() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT06 Save User Story Without Business Benefit For 'Save' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillEpicField("Case Management");
        pp.FillSearchField("System");
        mm.FillField(pp.TaskField, "Test Text");
        pp.SaveButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageBusinessBenefitField);
        System.out.println("[INFO]: Test 'GAT06 Save User Story Without Business Benefit For 'Save' button' - PASSED <<<");
    }

    @Test
    public void GAT07_SaveEmptyUserStoryForm(){
        System.out.println("[INFO]: Test 'GAT07 Save Empty User Story For 'Save & New' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        pp.SaveAndNewButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageActorField);
        pp.ValidateErrorMessage(pp.ErrorMessageBusinessBenefitField);
        pp.ValidateErrorMessage(pp.ErrorMessageEpicField);
        pp.ValidateErrorMessage(pp.ErrorMessageStorySubjectField);
        pp.ValidateErrorMessage(pp.ErrorMessageTaskField);
        System.out.println("[INFO]: Test 'GAT07 Save Empty User Story For 'Save & New' button' - PASSED <<<");
    }

    @Test
    public void GAT08_SaveUserStoryWithoutSubject() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT08 Save User Story Without Subject For 'Save & New' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        pp.FillEpicField("Case Management");
        pp.FillSearchField("System");
        mm.FillField(pp.TaskField, "Test Text");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveAndNewButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageStorySubjectField);
        System.out.println("[INFO]: Test 'GAT08 Save User Story Without Subject For 'Save & New' button' - PASSED <<<");
    }

    @Test
    public void GAT09_SaveUserStoryWithoutEpic() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT09 Save User Story Without Epic For 'Save & New' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillSearchField("System");
        mm.FillField(pp.TaskField, "Test Text");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveAndNewButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageEpicField);
        System.out.println("[INFO]: Test 'GAT09 Save User Story Without Epic For 'Save & New' button' - PASSED <<<");
    }

    @Test
    public void GAT10_SaveUserStoryWithoutActor(){
        System.out.println("[INFO]: Test 'GAT10 Save User Story Without Actor For 'Save & New' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillEpicField("Case Management");
        mm.FillField(pp.TaskField, "Test Text");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveAndNewButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageActorField);
        System.out.println("[INFO]: Test 'GAT10 Save User Story Without Actor For 'Save & New' button' - PASSED <<<");
    }

    @Test
    public void GAT11_SaveUserStoryWithoutTask() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT11 Save User Story Without Task For 'Save & New' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillEpicField("Case Management");
        pp.FillSearchField("System");
        mm.FillField(pp.BusinessBenefitField, "Business Test Text");
        pp.SaveAndNewButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageTaskField);
        System.out.println("[INFO]: Test 'GAT11 Save User Story Without Task For 'Save & New' button' - PASSED <<<");
    }

    @Test
    public void GAT12_SaveUserStoryWithoutBusinessBenefit() throws InterruptedException {
        System.out.println("[INFO]: Test 'GAT12 Save User Story Without Business Benefit For 'Save & New' button' - START >>>");
        pp.OpenNewUserStoryWindow();
        mm.FillField(pp.StorySubjectField, "Test Subject");
        pp.FillEpicField("Case Management");
        pp.FillSearchField("System");
        mm.FillField(pp.TaskField, "Test Text");
        pp.SaveAndNewButton.click();
        pp.ValidateErrorMessage(pp.ErrorMessageBusinessBenefitField);
        System.out.println("[INFO]: Test 'GAT12 Save User Story Without Business Benefit For 'Save & New' button' - PASSED <<<");
    }
}
